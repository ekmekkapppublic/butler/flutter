import 'package:butler/src/ButlerWorker.dart';

class Butler
{

  static String mainURL = "";
  static String _port = "";
  static String URL = "http://"+mainURL+":"+_port;




  static Initialize(ip,port)
  {
    mainURL = ip;
    _port = port;
  }


  ButlerWorker Post()
  {
      return ButlerWorker.POST().Initialize(mainURL, _port);
  }
  ButlerWorker Get()
  {
    return ButlerWorker.GET().Initialize(mainURL, _port);
  }
  ButlerWorker Delete()
  {
    return ButlerWorker.DELETE().Initialize(mainURL, _port);
  }
  ButlerWorker Put()
  {
    return ButlerWorker.PUT().Initialize(mainURL, _port);
  }
  ButlerWorker Form()
  {
    return ButlerWorker.FORM().Initialize(mainURL, _port);
  }
}